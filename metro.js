//Moyenne
function moyenne(valeures) {
    var moy = 0;
    //Calcul moyenne
    for (var i = 0; i < valeures.length; i++) {
        moy = (valeures[i] / valeures.length) + moy;
    }
    return moy;
}

//Calcul de l'ecart type
function ecart_type(valeures) {
    var moy = moyenne(valeures);
    var ec_type = 0;
    //Calcul ecart type
    for (var i = 0; i < valeures.length; i++) {
        ec_type = (Math.pow(Math.abs(valeures[i] - moy), 2) / (valeures.length - 1)) + ec_type;
    }
    ec_type = Math.sqrt(ec_type);
    return ec_type;
}

//Fonction résolution
function resolution(valeure) {
    var i = 0;
    while (valeure - Math.trunc(valeure) != 0) {
        valeure = 10 * valeure;
        i++;
    }
    return 0.5 * Math.pow(10, -i);
}

//Extrait des valeurs d'une chaine marqué par un séparateur
function extrait_separateur(chaine, separateur) {
    var extraction = new Array();
    var curseur;
    var i = 0;
    while (chaine.length > 0) {
        curseur = chaine.indexOf(";");
        extraction[i] = chaine.substring(0, curseur);
        chaine = chaine.substring(curseur + 1);
        i++;
    }
    return extraction;
}

//conversion radians/degres
function toDeg(angle) {
    return angle * 180 / Math.PI;
}

function toRad(angle) {
    return angle * Math.PI / 180;
}

//Recupere les valeures puis stock dans tableau
function recup_val(element) {
    var identifiant = element.getAttribute("id");
    var chaine = document.getElementById(identifiant + "_input").value;
    chaine = chaine + ";";
    var valeures = extrait_separateur(chaine, ";");
    // **********temporaire************ l'affichage sera deporter plus tard
    document.getElementById("valeur_aff").innerHTML = "Valures :";
    for (var i = 0; i < valeures.length; i++) document.getElementById("valeur_aff").innerHTML += "<br>" + valeures[i];
    document.getElementById("nombre_val").innerHTML = "Nombre de valeures :&nbsp;" + valeures.length;
    document.getElementById("affich_ect").innerHTML = "Votre &eacute;cart-type ici :&nbsp;" + ecart_type(valeures);
    document.getElementById("affich_moy").innerHTML = "Votre moyenne ici :&nbsp;" + moyenne(valeures);
    document.getElementById("affich_res").innerHTML = "Votre r&eacute;solution ici :&nbsp;" + resolution(valeures[0]);
}