function loi_d_ohm() 
{
    // U=R*I
    var U = document.getElementById("LO_U").value;
    var R = document.getElementById("LO_R").value;
    var I = document.getElementById("LO_I").value;

    if (R && I) U = R * I;
    else if (R && U) I = U / R;
    else if (I && U) R = U / I;

    document.getElementById("LO_U").value = U;
    document.getElementById("LO_R").value = R;
    document.getElementById("LO_I").value = I;

}

window.onload = function() {

var step = Math.pow(10, .05);
var chart = new CanvasJS.Chart("chartContainer", {
	zoomEnabled: true,
	zoomType: "xy",
	exportEnabled: true,
	title: {
		text: "Gain"
	},
	axisX: {
		logarithmic: true,
		title: "Fréquence f(Hz)",
		minimum: .01,
		suffix: "Hz",
		stripLines: [{
			value: 1,
			label: "Fréquence de coupure",
			labelFontColor: "#808080",
			labelAlign: "near"
		}]
	},
	axisY: {
		title: "Gain (dB)",
		titleFontColor: "#4F81BC",
		labelFontColor: "#4F81BC"
	},
	axisY2: {
		title: "𝛗 (deg)",
		titleFontColor: "#C0504E",
		labelFontColor: "#C0504E"
	},
	toolTip: {
		shared: true
	},
	legend:{
		cursor:"pointer",
		itemclick: toogleDataSeries
	},
	data: [{
		type: "line",
		name: "Gain",
		showInLegend: true,
		yValueFormatString: "#,##0.00 db",
		xValueFormatString: "f = #,##0.00#Hz",
		dataPoints: type1DataPoints(step)
	},
	{
		type: "line",
		name: "Phase",
		color: "#C0504E",
		showInLegend: true,
		axisYType: "secondary",
		yValueFormatString: "#,##0.00 deg",
		xValueFormatString: "f = #,##0.00#Hz",
		dataPoints: type2DataPoints(.02, step)
	}]
});

chart.render();

function type1DataPoints(step){//rajouter pas suivant max
	var dataPoints = [];
	var h;
	for(var w = .01; w < 100 ; w *= step){
		h =  -5 * Math.log(w*w + 1);
		dataPoints.push({x: w, y: h});
	}
	return dataPoints
}

function type2DataPoints(e, step){
	var dataPoints = [];
	var h;
	for(var w = .01; w < 100 ; w *= step){
		h =  -5 * Math.log(Math.pow((1 - w*w), 2) + 4 *e*e*w*w);
		dataPoints.push({x: w, y: h});
	}
	return dataPoints;
}

function toogleDataSeries(e){
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else{
		e.dataSeries.visible = true;
	}
	chart.render();
}

}
