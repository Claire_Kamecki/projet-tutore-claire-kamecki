function dilatation_thermique(){
    //On recupere les valeurs
    var alpha = document.getElementById("DT_A").value;
    var lo = document.getElementById("DT_Lo").value;
    var l = document.getElementById("DT_L").value;
    var T = document.getElementById("DT_T").value;
    var To = document.getElementById("DT_To").value;
    
    //On fait les calculs
    if(lo && l && T && To) alpha = (l - lo) / (lo * (T - To));
    else if (alpha && l&& T&& To) lo = l / (1-alpha * (T - To));
    else if (alpha && lo && T && To) l = parseFloat(lo) + ( alpha * lo * (T - To)) ;
    else if (alpha && l && lo && To) T = parseFloat(To) + ((l - lo) / (alpha * lo)) ;
    else if (alpha && l && T && lo) To = parseFloat(T)+ ((lo - l) / (lo * alpha));
    
    //On renvoi les valeurs
    document.getElementById("DT_A").value = alpha;
    document.getElementById("DT_Lo").value = lo;
    document.getElementById("DT_Lo-2").value = lo;
    document.getElementById("DT_L").value = l;
    document.getElementById("DT_T").value = T;
    document.getElementById("DT_To").value = To;
}

function degre_kelvin(){
    var deg = document.getElementById("DK_C").value;
    var kelvin = document.getElementById("DK_K").value;
    
    if (deg) kelvin = parseFloat(deg) + 273.15;
    else if (kelvin) deg = parseFloat(kelvin) - 273.15;
    
    document.getElementById("DK_C").value = deg;
    document.getElementById("DK_K").value = kelvin;
}

function gaz_parfait(){
    var P = document.getElementById("GP_P").value;
    var V = document.getElementById("GP_V").value;
    var n = document.getElementById("GP_N").value;
    var T = document.getElementById("GP_T").value;
    
    if (V&&n&&T) P=(n*8.32*T)/V;
    else if(P&&n&&T) V=(n*8.32*T)/P;
    else if(P&&V&&T) n=(P*V)/(8.32*T);
    else if(P&&V&&n) T=(P*V)/(8.32*n);
    
    document.getElementById("GP_P").value = P;
    document.getElementById("GP_V").value = V;
    document.getElementById("GP_N").value = n;
    document.getElementById("GP_T").value = T;
}

function chaleur_travail(){
    var W = document.getElementById("CT_W").value;
    var Q = document.getElementById("CT_Q").value;
    
    if (W) Q=W/4.1855;
    else if (Q) W=Q*4.1855;
    
    document.getElementById("CT_W").value = W;
    document.getElementById("CT_Q").value = Q;
    
}

function chaleur_massique(){
    var Cp = document.getElementById("CM_CP").value;
    var m = document.getElementById("CM_M").value;
    var dQ = document.getElementById("CM_DQ").value;
    var dT = document.getElementById("CM_DT").value;
    
    if (m&&dQ&&dT) Cp=(1/m)*(dQ/dT);
    else if(Cp&&dQ&&dT) m=dQ/(Cp*dT);
    else if(Cp&&m&&dT) dQ=Cp*dT*m;
    else if(Cp&&m&&dQ) dT=dQ/(Cp*m);
    
    
    document.getElementById("CM_CP").value = Cp;
    document.getElementById("CM_M").value = m;
    document.getElementById("CM_DQ").value = dQ;
    document.getElementById("CM_DT").value = dT;

    
}

function chaleur_latente(){
    var Q = document.getElementById("CL_Q").value;
    var m = document.getElementById("CL_M").value;
    var L = document.getElementById("CL_L").value;
  
    if (m && L) Q = m * L;
    else if (Q && L) m = Q / L;
    else if (Q && m) L = Q * m;
  
    document.getElementById("CL_Q").value = Q;
    document.getElementById("CL_M").value = m;
    document.getElementById("CL_L").value = L;
    
}

function transformation_isotherme(){
    var dW =  document.getElementById("TI_DW").value;
    var n =  document.getElementById("TI_N").value;
    var T1 =  document.getElementById("TI_T1").value;
    
    var V1 =  document.getElementById("TI_V1").value;
    var V2 =  document.getElementById("TI_V2").value;

    if (n&&T1&&V1&&V2) dW=1*(n*8.32*T1*Math.log(V2/V1));
    else if (dW&&T1&&V1&&V2) n=dW/(8.32*T1*Math.log(V2/V1));
    else if (n&&dW&&V1&&V2) T1=dW/(8.32*n*Math.log(V2/V1));
    else if (n&&T1&&dW&&V2) V1=Math.exp((-dW/(n*8.32*T1))+Math.log(V2));
    else if (n&&T1&&dW&&V1) V2=Math.exp((dW/(n*8.32*T1))+Math.log(V1));
    
    document.getElementById("TI_DW").value = dW;
    document.getElementById("TI_N").value = n;
    document.getElementById("TI_T1").value = T1;
    document.getElementById("TI_V1").value = V1;
    document.getElementById("TI_V2").value = V2;
    
}