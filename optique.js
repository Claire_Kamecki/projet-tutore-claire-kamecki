/******************
Optique geometrique
******************/

function pouvoir_reflecteur() {
    //On recupere les valeures
    var R = document.getElementById("PR_R").value;
    var n1 = document.getElementById("PR_n1").value;
    var n2 = document.getElementById("PR_n2").value;
    //On fait les calculs
    if (n1 && n2) R = Math.pow((n1 - n2) / (n1 + n2), 2);
    else if (R && n1) n2 = (-Math.sqrt(R) + 1) / (n1 * (Math.sqrt(R) - 1));
    else if (R && n2) n1 = (-Math.sqrt(R) + 1) / (n2 * (Math.sqrt(R) - 1));
    //On renvoi les valeures
    document.getElementById("PR_R").value = R;
    document.getElementById("PR_n1").value = n1;
    document.getElementById("PR_n2").value = n2;
    document.getElementById("PR_n1-2").value = n1;
    document.getElementById("PR_n2-2").value = n2;
}

function refraction() {
    //On recupere les valeures
    var n1 = document.getElementById("R_n1").value;
    var i1 = toRad(document.getElementById("R_i1").value);
    var n2 = document.getElementById("R_n2").value;
    var i2 = toRad(document.getElementById("R_i2").value);
    //On fait les calculs
    if (n2 && i2 && i1) n1 = n2 * Math.sin(i2) / Math.sin(i1);
    else if (n2 && i2 && n1) i1 = Math.asin(n2 * Math.sin(i2) / n1);
    else if (n1 && i1 && i2) n2 = n1 * Math.sin(i1) / Math.sin(i2);
    else if (n1 && i1 && n2) i2 = Math.asin(n1 * Math.sin(i1) / n2);
    //On renvoi les valeures
    document.getElementById("R_n1").value = n1;
    document.getElementById("R_i1").value = toDeg(i1);
    document.getElementById("R_n2").value = n2;
    document.getElementById("R_i2").value = toDeg(i2);
}

function diametre_apparant() {
    //On recupere les valeures
    var alpha = document.getElementById("DA_alpha").value;
    var h = document.getElementById("DA_h").value;
    var d = document.getElementById("DA_d").value;
    //On fait les calculs
    if (h && d) alpha = Math.atan(h / d);
    else if (alpha && d) h = d * Math.tan(alpha);
    else if (h && alpha) d = h / Math.tan(alpha);
    //On renvoi les valeures
    document.getElementById("DA_alpha").value = alpha;
    document.getElementById("DA_h").value = h;
    document.getElementById("DA_d").value = d;
}

function grandissement() {
    //On recupere les valeures
    var gama = document.getElementById("Gd_gama").value;
    var ab = document.getElementById("Gd_ab").value;
    var cd = document.getElementById("Gd_cd").value;
    //On fait les calculs
    if (cd && ab) gama = cd / ab;
    else if (gama && ab) cd = gama * ab;
    else if (cd && gama) ab = cd / gama;
    //On renvoi les valeures
    document.getElementById("Gd_gama").value = gama;
    document.getElementById("Gd_ab").value = ab;
    document.getElementById("Gd_cd").value = cd;
}

function grossissement_commercial() {
    //On recupere les valeures
    var gc = document.getElementById("GC_gc").value;
    var alpha = document.getElementById("GC_alpha").value;
    var alpha_c = document.getElementById("GC_alpha_c").value;
    var PP = 0.25;
    var f = document.getElementById("GC_f").value;
    //On fait les calculs
    if (alpha && alpha_c) {
        gc = alpha / alpha_c;
        f = PP / gc;
    } else if (f) {
        gc = PP / f;
    }
    //On renvoi les valeures
    document.getElementById("GC_gc").value = gc;
    document.getElementById("GC_alpha").value = alpha;
    document.getElementById("GC_alpha_c").value = alpha_c;
    document.getElementById("GC_f").value = f;
}

function grossissement() {
    //On recupere les valeures
    var g = document.getElementById("Gs_g").value;
    var alpha_1 = document.getElementById("Gs_alpha_1").value;
    var alpha_2 = document.getElementById("Gs_alpha_2").value;
    var f1 = document.getElementById("Gs_f1").value;
    var f2 = document.getElementById("Gs_f2").value;
    //On fait les calculs
    if (alpha_1 && alpha_2) g = alpha_1 / alpha_2;
    else if (f1 && f2) g = f1 / f2;
    //On renvoi les valeures
    document.getElementById("Gs_g").value = g;
    document.getElementById("Gs_alpha_1").value = alpha_1;
    document.getElementById("Gs_alpha_2").value = alpha_2;
    document.getElementById("Gs_f1").value = f1;
    document.getElementById("Gs_f2").value = f2;
}

/******************
Optique ondulatoire
******************/

function efficacite_lumineuse() {
    //On recupere les valeures
    var k = document.getElementById("EL_k").value;
    var fluxL = document.getElementById("EL_fluxL").value;
    var fluxE = document.getElementById("EL_fluxE").value;
    //On fait les calculs
    if (fluxE && fluxL) k = fluxL / fluxE;
    else if (fluxE && k) fluxL = fluxE * k;
    else if (fluxL && k) fluxE = fluxL / k;
    //On renvoi les valeures
    document.getElementById("EL_k").value = k;
    document.getElementById("EL_fluxL").value = fluxL;
    document.getElementById("EL_fluxE").value = fluxE;
}

function rendement() {
    //On recupere les valeures
    var r = document.getElementById("Rd_r").value;
    var fluxL = document.getElementById("Rd_fluxL").value;
    var p = document.getElementById("Rd_p").value;
    //On fait les calculs
    if (fluxL && p) r = fluxL / p;
    else if (p && r) fluxL = p * r;
    else if (fluxL && r) p = fluxL / r;
    //On renvoi les valeures
    document.getElementById("Rd_r").value = r;
    document.getElementById("Rd_fluxL").value = fluxL;
    document.getElementById("Rd_p").value = p;
}

function angle_solide() {
    //On recupere les valeures
    var omega = document.getElementById("AS_omega").value;
    var aire = document.getElementById("AS_aire").value;
    var r = document.getElementById("AS_r").value;
    //On fait les calculs
    if (r && aire) omega = aire / Math.pow(r, 2);
    else if (omega && r) aire = omega * Math.pow(r, 2);
    else if (aire && omega) r = Math.sqrt(aire / omega);
    //On renvoi les valeures
    document.getElementById("AS_omega").value = omega;
    document.getElementById("AS_aire").value = aire;
    document.getElementById("AS_r").value = r;
}

function variation_angle_solide() {
    //On recupere les valeures
    var domega = document.getElementById("VAS_domega").value;
    var teta = toRad(document.getElementById("VAS_teta").value);
    var dS = document.getElementById("VAS_dS").value;
    var r = document.getElementById("VAS_r").value;
    //On fait les calculs
    if (teta && dS && r) domega = Math.cos(teta) * dS / Math.pow(r, 2);
    else if (teta && dS && domega) r = Math.sqrt(Math.cos(teta) * dS / domega);
    else if (domega && teta && r) dS = domega * Math.pow(r, 2) / Math.cos(teta);
    else if (domega && r && dS) teta = Math.acos(domega * Math.pow(r, 2) / dS);
    //On renvoi les valeures
    document.getElementById("VAS_domega").value = domega;
    document.getElementById("VAS_teta").value = toDeg(teta);
    document.getElementById("VAS_dS").value = dS;
    document.getElementById("VAS_r").value = r;
}

function intensite_lumineuse() {
    //On recupere les valeures
    var il = document.getElementById("IL_il").value;
    var dFluxL = document.getElementById("IL_dfluxL").value;
    var dOmega = document.getElementById("IL_dOmega").value;
    //On fait les calculs
    if (dFluxL && dOmega) il = dFluxL / dOmega;
    else if (dOmega && il) dFluxL = dOmega * il;
    else if (dFluxL && il) dOmega = dFluxL / il;
    //On renvoi les valeures
    document.getElementById("IL_il").value = il;
    document.getElementById("IL_dfluxL").value = dFluxL;
    document.getElementById("IL_dOmega").value = dOmega;

}

function intensite_source_lambertienne() {
    //On recupere les valeures
    var i = document.getElementById("ISL_i").value;
    var io = document.getElementById("ISL_io").value;
    var teta = toRad(document.getElementById("ISL_teta").value);
    //On fait les calculs
    if (io && teta) i = io * Math.cos(teta);
    else if (i && teta) io = i / Math.cos(teta);
    else if (i && io) teta = Math.acos(i / io);
    //On renvoi les valeures
    document.getElementById("ISL_i").value = i;
    document.getElementById("ISL_io").value = io;
    document.getElementById("ISL_teta").value = toDeg(teta);
}

function intensite_source_lambertienne_uniforme() {
    //On recupere les valeures
    var i = document.getElementById("ISLU_i").value;
    var l = document.getElementById("ISLU_l").value;
    var s = document.getElementById("ISLU_s").value;
    var teta = toRad(document.getElementById("ISLU_teta").value);
    //On fait les calculs
    if (l && s && teta) i = l * s * Math.cos(teta);
    else if (i && s && teta) l = i / (s * Math.cos(teta));
    else if (i && l && teta) s = i / (l * Math.cos(teta));
    else if (i && s && l) teta = Math.acos(i / (s * l));
    //On renvoi les valeures
    document.getElementById("ISLU_i").value = i;
    document.getElementById("ISLU_l").value = l;
    document.getElementById("ISLU_s").value = s;
    document.getElementById("ISLU_teta").value = toDeg(teta);
}

function eclairement() {
    //On recupere les valeures
    var El = document.getElementById("Ec_el").value;
    var dFluxL = document.getElementById("Ec_dfluxL").value;
    var dSr = document.getElementById("Ec_dSr").value;
    //On fait les calculs
    if (dFluxL && dSr) El = dFluxL / dSr;
    else if (dSr && El) dFluxL = dSr * El;
    else if (dFluxL && El) dSr = dFluxL / El;
    //On renvoi les valeures
    document.getElementById("Ec_el").value = El;
    document.getElementById("Ec_dfluxL").value = dFluxL;
    document.getElementById("Ec_dSr").value = dSr;
}

function eclairement_source_ponctuelle() {
    //On recupere les valeures
    var E = document.getElementById("ESP_e").value;
    var Iteta = toRad(document.getElementById("ESP_iteta").value);
    var tetaprime = toRad(document.getElementById("ESP_tetaprime").value);
    var R = document.getElementById("ESP_r").value;
    //On fait les calculs
    if (Iteta && tetaprime && R) E = Iteta * Math.cos(tetaprime) / Math.pow(R, 2);
    else if (E && R && tetaprime) Iteta = (E * Math.pow(R, 2)) / (Math.cos(tetaprime));
    else if (Iteta && tetaprime && E) R = Math.sqrt((Iteta * Math.cos(tetaprime)) / E);
    else if (R && E && Iteta) tetaprime = Math.acos(Math.pow(R, 2) * E / Iteta);
    //On renvoi les valeures
    document.getElementById("ESP_e").value = E;
    document.getElementById("ESP_iteta").value = toDeg(Iteta);
    document.getElementById("ESP_tetaprime").value = toDeg(tetaprime);
    document.getElementById("ESP_r").value = R;
}